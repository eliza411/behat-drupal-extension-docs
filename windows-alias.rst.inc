.. todo:: replace this with an alias file. Look into other alias files.         
                                                                                
   **Creating the alias**                                                       
                                                                                
   1. Start the registry editor, regedit.exe                                    
   2. From the Edit menu, select New > Key                                      
   3. Enter the name of the alias, behat, and press Enter                       
   4. Move to the new key and double clock on the (Default) value, which is     
      blank by default.                                                         
                                                                                
   5. Move to HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths
   6. Set the fully qualified file name, c:\opt\bin\behat                       
   7. Close the registry editor  
