\select@language {english}
\contentsline {chapter}{\numberline {1}Testing your site with the Drupal Extension to Behat and Mink}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}What do Behat and Mink Do?}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}What does the Drupal Extension add?}{3}{section.1.2}
\contentsline {chapter}{\numberline {2}System Requirements}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Meet the system requirements}{5}{section.2.1}
\contentsline {chapter}{\numberline {3}Stand-alone installation}{7}{chapter.3}
\contentsline {chapter}{\numberline {4}Windows Installation: WAMP}{11}{chapter.4}
\contentsline {section}{\numberline {4.1}System Requirements}{11}{section.4.1}
\contentsline {chapter}{\numberline {5}System-wide installation}{13}{chapter.5}
\contentsline {section}{\numberline {5.1}Overview}{13}{section.5.1}
\contentsline {section}{\numberline {5.2}Install Composer}{13}{section.5.2}
\contentsline {section}{\numberline {5.3}Install the Drupal Extension}{13}{section.5.3}
\contentsline {section}{\numberline {5.4}Set up tests}{14}{section.5.4}
\contentsline {chapter}{\numberline {6}Drupal Extension Drivers}{17}{chapter.6}
\contentsline {chapter}{\numberline {7}Blackbox Driver}{19}{chapter.7}
\contentsline {section}{\numberline {7.1}Region steps}{19}{section.7.1}
\contentsline {section}{\numberline {7.2}Example:}{19}{section.7.2}
\contentsline {chapter}{\numberline {8}Drush Driver}{23}{chapter.8}
\contentsline {section}{\numberline {8.1}Install Drush}{23}{section.8.1}
\contentsline {section}{\numberline {8.2}Create a Drush alias}{23}{section.8.2}
\contentsline {section}{\numberline {8.3}Enable the Drush driver in the behat.yml}{23}{section.8.3}
\contentsline {section}{\numberline {8.4}Calling the Drush driver}{24}{section.8.4}
\contentsline {chapter}{\numberline {9}Drupal API Driver}{27}{chapter.9}
\contentsline {section}{\numberline {9.1}Enable the Drupal API Driver}{27}{section.9.1}
\contentsline {chapter}{\numberline {10}Contributed Module Subcontexts}{29}{chapter.10}
\contentsline {section}{\numberline {10.1}Discovering SubContexts}{29}{section.10.1}
\contentsline {section}{\numberline {10.2}Disable autoloading}{29}{section.10.2}
\contentsline {section}{\numberline {10.3}For Contributors}{30}{section.10.3}
