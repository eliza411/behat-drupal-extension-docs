Windows Installation: WAMP
==========================

There are many ways to set up a Windows system with all the components
necessary. This set of instructions aims to take a clean Windows install
and put together all the components for testing.


.. note:: You can set up your system in many other ways. These directions 
   reference WAMP directories and the Git Shell from msysgit.   

System Requirements
-------------------
1. WAMP (For Drupal): http://www.wampserver.com/en

   .. note::

       * WAMP Requires Visual C++ Redistributable for Visual Studio 2012 Update 4
         http://www.microsoft.com/en-us/download/details.aspx?id=30679

       * Skype sometimes blocks port 80, preventing the web server from starting. 
         Quit Skype and restart WAMP services if you run into this.

       * Beware of your PHP verions. The command-line and web server can differ 
         which manifests in unpredictable Behat behavior. 

       * Behat requires AT LEAST 5.3.5, which means tests may need to be run with a
         different version of PHP than a Drupal 5 site.

       * If you run into a MSVCR100.DLL error, see 
         http://forum.wampserver.com/read.php?2,84961

2. Composer: https://getcomposer.org/doc/00-intro.md#using-the-installer

   .. note:: The path to PHP for a default WAMP installations is 
      c:\\wamp\bin\php5.5.12\php.exe  Adjust your version number accordingly.

3. Git: http://msysgit.github.io/ Accepted defaults.

4. Drush: http://www.drush.org/drush_windows_installer

   .. note:: Choose "Register Environment Variable" in order to run Drush from
      within the Git Bash shell.

   .. todo:: figure why this didn't work with Windows 8

5.  Follow the [local]
    (http://behat-drupal-extension.readthedocs.org/globalinstall.html#install-the-drupal-extension) 
    directions to Install the Drupal Extension.

6. Java (for Selenium): http://www.java.com/en/download

7. Selenium (for running a browser): http://docs.seleniumhq.org/download

   .. note:: Get the version under the second heading, Selenium Server
      formerly the Selenium RC Server), which is currently 2.42.2  To start it, 
      locate the .jar file after download and double-click.

   
    

